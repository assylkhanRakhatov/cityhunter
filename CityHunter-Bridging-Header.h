//
//  CityHunter-Bridging-Header.h
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 28.08.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

#ifndef CityHunter_CityHunter_Bridging_Header_h
#define CityHunter_CityHunter_Bridging_Header_h
#define MR_SHORTHAND

#import "VKActivity.h"
#import "CoreData+MagicalRecord.h"
#import "MagicalRecord.h"
#import "MagicalRecord+Actions.h"
#import "MagicalRecord+ErrorHandling.h"
#import "MagicalRecord+Options.h"
#import "MagicalRecord+ShorthandSupport.h"
#import "MagicalRecord+Setup.h"
#import "MagicalRecord+iCloud.h"

#import "NSManagedObject+MagicalRecord.h"
#import "NSManagedObject+MagicalRequests.h"
#import "NSManagedObject+MagicalFinders.h"
#import "NSManagedObject+MagicalAggregation.h"
#import "NSManagedObjectContext+MagicalRecord.h"
#import "NSManagedObjectContext+MagicalObserving.h"
#import "NSManagedObjectContext+MagicalSaves.h"
#import "NSManagedObjectContext+MagicalThreading.h"
#import "NSPersistentStoreCoordinator+MagicalRecord.h"
#import "NSManagedObjectModel+MagicalRecord.h"
#import "NSPersistentStore+MagicalRecord.h"

#import "MagicalImportFunctions.h"
#import "NSManagedObject+MagicalDataImport.h"
#import "NSNumber+MagicalDataImport.h"
#import "NSObject+MagicalDataImport.h"
#import "NSString+MagicalDataImport.h"
#import "NSAttributeDescription+MagicalDataImport.h"
#import "NSRelationshipDescription+MagicalDataImport.h"
#import "NSEntityDescription+MagicalDataImport.h"
#import "SystemConfiguration.h"
//#import "Reachability.h"

#endif

#import "VKProvider.h"
