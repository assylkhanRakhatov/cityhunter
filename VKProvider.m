//
//  VKVideoProvider.m
//  Nurline
//
//  Created by Yekaterina Gekkel on 18.12.14.
//  Copyright (c) 2014 SimpleCode. All rights reserved.
//

#import "VKProvider.h"


#define VK_APP_ID @"4685410"

@interface VKProvider ()

@property (nonatomic, strong) VKAccessToken *token;


@end

@implementation VKProvider

#pragma mark - Initialization

+ (instancetype)instance {
    static VKProvider *_instance;
    @synchronized(self) {
        if (!_instance)
            _instance = [[self alloc] init];
        return _instance;
    }
}

#pragma mark - Perform to delegates

- (void)performVKProviderShouldPresentViewController:(UIViewController *)viewController {
    if ([_delegate respondsToSelector:@selector(vkProviderShouldPresentViewController:)])
        [_delegate performSelector:@selector(vkProviderShouldPresentViewController:) withObject:viewController];
}

#pragma mark - Public Methods

- (void)authorize {
    [VKSdk initializeWithDelegate:self
                         andAppId:VK_APP_ID];
    if ([VKSdk wakeUpSession])
    {
        _token = [VKSdk getAccessToken];
    } else {
        [VKSdk authorize:@[VK_PER_WALL, VK_PER_PHOTOS, VK_PER_NOHTTPS, VK_PER_EMAIL, VK_PER_MESSAGES, VK_PER_VIDEO]
            revokeAccess:YES
              forceOAuth:YES];
        
    }
}

#pragma mark - VK Delegates

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
//    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
//    [vc presentIn:self];
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {
    [self authorize];
}

- (void)vkSdkReceivedNewToken:(VKAccessToken *)newToken {
//    [self startWorking];
}

- (void)vkSdkAcceptedUserToken:(VKAccessToken *)token {
//    [self startWorking];
}
- (void)vkSdkUserDeniedAccess:(VKError *)authorizationError {
    [[[UIAlertView alloc] initWithTitle:nil message:@"Access denied" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self performVKProviderShouldPresentViewController:controller];
}

@end
