//
//  VKVideoProvider.h
//  Nurline
//
//  Created by Yekaterina Gekkel on 18.12.14.
//  Copyright (c) 2014 SimpleCode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VKSdk.h"

@protocol VKProviderDelegate;

@interface VKProvider : NSObject <VKSdkDelegate>

+ (instancetype)instance;
- (void)authorize;


@property (nonatomic, strong) NSObject<VKProviderDelegate> *delegate;

@end

@protocol VKProviderDelegate <NSObject>

@required

- (void)vkProviderShouldPresentViewController:(UIViewController *)controller;


@end
