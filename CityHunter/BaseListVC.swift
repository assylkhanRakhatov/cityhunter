//
//  BaseListVC.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 17.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

protocol BaseListVC {
	var className: String {
		get set
	}
	
	weak var errorLabel: UILabel! {
		get set
	}
	weak var tableView: UITableView! {
		get set
	}
	var indicator: UIActivityIndicatorView! {
		get set
	}
}
