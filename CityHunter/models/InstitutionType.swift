//
//  InstitutionType.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 31.08.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation

class InstitutionType {
	var id: Int!
	var name: String = ""
	var imageName: String = ""

	init(id: Int, name: String, imageName: String){
		self.id = id
		self.name = name
		self.imageName = imageName
	}
	
	init(){
		
	}
}
