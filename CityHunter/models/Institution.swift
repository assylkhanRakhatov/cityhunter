//
//  Institution.swift
//
//
//  Created by Assylkhan Rakhatov on 28.08.15.
//
//

import Foundation
import CoreData
import SwiftyJSON

class Institution {
  var id: Int!
  var isRec: Bool?
  var title: String = ""
  var date: String = ""
  var thumb: String = ""
  var imageUrls: [String] = []
  var shortDescription: String = ""
  var description: String = ""
  var address: String = ""
  var workTime: String = ""
  var info: [[String : String]]?
  var institutionType: InstitutionType?
  var coordinates: [String: Double] = [String: Double]()
  
  //	init(id: Int, title: String, date: String, thumb: String
  //		, shortDescription: String, address: String, institutionType: InstitutionType){
  //		self.id = id
  //		self.title = title
  //		self.date = date
  //		self.thumb = thumb
  //		self.shortDescription = shortDescription
  //		self.address = address
  //		self.institutionType = institutionType
  //	}
  //
  //	init(id: Int, title: String, address: String, workTime: String,
  //			info: [[String : String]],
  //			description: String,
  //			images: [String], date: String) {
  //			self.id = id
  //			self.title = title
  //			self.address = address
  //			self.workTime = workTime
  //			self.info = info
  //			self.description = description
  //			self.imageUrls = images
  //			self.date = date
  //	}
  
  init() {
    
  }
  
  init(jsonForShort: JSON) {
    var title = StringUtil.clearString(jsonForShort["title"].stringValue)
    title = title.stringByReplacingOccurrencesOfString("&quot;", withString: "\"", options: NSStringCompareOptions.LiteralSearch, range: nil)
    title = title.stringByReplacingOccurrencesOfString("&laquo;", withString: "\"", options: NSStringCompareOptions.LiteralSearch, range: nil)
    title = title.stringByReplacingOccurrencesOfString("&raquo;", withString: "\"", options: NSStringCompareOptions.LiteralSearch, range: nil)
    let shortDescription = StringUtil.clearString(jsonForShort["short_description"].stringValue)
    
    self.id = jsonForShort["id"].intValue
    self.isRec = jsonForShort["is_rec"].boolValue
    self.title = title
    self.date = jsonForShort["date"].stringValue
    self.thumb = jsonForShort["thumb"].stringValue
    self.shortDescription = shortDescription
    self.address = jsonForShort["address"].stringValue
    self.institutionType = InstitutionType()
    
  }
  
  init(jsonForFull: JSON) {
    if let jsonArray = jsonForFull["info"].array {
      info = [[String : String]]()
      print("json array : \(jsonArray)")
      for (_, item) in jsonArray.enumerate() {
        var caption = item["caption"].stringValue
        caption = caption.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        var value = item["value"].stringValue
        value = StringUtil.clearString(value)
        
        var dict = ["caption" : caption]
        dict["value"] =  value
        info!.append(dict)
        
      }
    }
    
    let description = StringUtil.clearString(jsonForFull["description"].stringValue)
    print("regexed description: \(description)")
    
    let title = StringUtil.clearString(jsonForFull["title"].stringValue)
    
    self.id = jsonForFull["id"].intValue
    self.title = title
    self.description = description
    for (_, element) in jsonForFull["images"] {
      imageUrls.append(element.stringValue)
    }
    if let coordinates = jsonForFull["coordinates"].dictionaryObject as? [String: Double] {
      self.coordinates = coordinates
    }
  }
  
}
