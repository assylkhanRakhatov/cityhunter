//
//  UIConstants.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 02.05.16.
//  Copyright © 2016 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

let recommendationColor = UIColor(red: 110/255.0, green: 1, blue: 117/255.0, alpha: 0.50)
