//
//  VkProvider.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 14.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation

class VKProvider: NSObject, VKSdkDelegate {
	
	var token: VKAccessToken?
	var facade: SCCameraFacade?
	
	class func instance() -> Self {
	}
	
	func authorize() {
	}
	
	func uploadVideo(video: String, withName name: String) {
	}
	
	weak var delegate: NSObject<VKProviderDelegate>?
}

protocol VKProviderDelegate: NSObject {
	func vkProviderShouldPresentViewController(controller: UIViewController)
	func vkProviderDidStartLoadingVideo(video: String)
	
	func vkProviderDidErrorLoadingVideo(video: String)
	
	func vkProviderDidEndLoadingVideo(video: String)
}