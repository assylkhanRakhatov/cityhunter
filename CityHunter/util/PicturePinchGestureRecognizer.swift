//
//  PicturePinchGestureRecognizer.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 25.07.16.
//  Copyright © 2016 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class PicturePinchGestureRecognizer: UIPinchGestureRecognizer {
  var minScale: CGFloat?
  var maxScale: CGFloat?
}

extension UIImageView {
  
  func makePinchable(minScale: CGFloat, maxScale: CGFloat) {
    let pinchRecognizer = PicturePinchGestureRecognizer(target: self, action: #selector(UIImageView.zoom(_:)))
    pinchRecognizer.minScale = minScale
    pinchRecognizer.maxScale = maxScale
    self.addGestureRecognizer(pinchRecognizer)
  }
  
  func zoom(sender: PicturePinchGestureRecognizer) {
    let minScale = sender.minScale!
    let maxScale = sender.maxScale!
    if sender.state == .Ended || sender.state == .Changed {
      
      let currentScale = self.frame.size.width / self.bounds.size.width
      var newScale = currentScale*sender.scale
      
      if newScale < minScale {
        newScale = minScale
      }
      if newScale > maxScale {
        newScale = maxScale
      }
      
      let transform = CGAffineTransformMakeScale(newScale, newScale)
      
      self.transform = transform
      sender.scale = 1
    }
  }
}
