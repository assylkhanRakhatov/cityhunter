//
//  ImageUtil.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 07.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class ImageUtil {
	
	static func getDataFromUrl(urL:NSURL, completion: ((data: NSData?) -> Void)) {
		NSURLSession.sharedSession().dataTaskWithURL(urL) { (data, response, error) in
			completion(data: data)
			}.resume()
	}
	
	static func downloadImage(url:NSURL, imageView: UIImageView){
		print("Started downloading \"\(url.URLByDeletingPathExtension)\".")
		getDataFromUrl(url) { data in
			dispatch_async(dispatch_get_main_queue()) {
				print("Finished downloading \"\(url.URLByDeletingPathExtension)\".")
				imageView.image = UIImage(data: data!)
			}
		}
	}
}