//
//  StringUtil.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 17.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation

class StringUtil {
	static func clearString(var value: String) -> String {
		value = value.stringByReplacingOccurrencesOfString("&quot;", withString: "\"", options: NSStringCompareOptions.LiteralSearch, range: nil)
		value = value.stringByReplacingOccurrencesOfString("&laquo;", withString: "\"", options: NSStringCompareOptions.LiteralSearch, range: nil)
		value = value.stringByReplacingOccurrencesOfString("&raquo;", withString: "\"", options: NSStringCompareOptions.LiteralSearch, range: nil)
		value = value.stringByReplacingOccurrencesOfString("&amp;", withString: "&", options: NSStringCompareOptions.LiteralSearch, range: nil)
		value = value.stringByReplacingOccurrencesOfString("&nbsp;", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
		value = value.stringByReplacingOccurrencesOfString("&mdash;", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
		value = value.stringByReplacingOccurrencesOfString("&ndash;", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
		value = value.stringByReplacingOccurrencesOfString("&rdquo;", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
		value = value.stringByReplacingOccurrencesOfString("&hellip;", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
		value = value.stringByReplacingOccurrencesOfString("&prime;", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        value = value.stringByReplacingOccurrencesOfString("&eacute;", withString: "é", options: NSStringCompareOptions.LiteralSearch, range: nil)
		value = value.replace("(\r\n){2,}", template: "\r\n")
		value = value.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "\r\n"))
//		value = value.replace("^(\r\n)+", template: "")
//		value = value.replace("(\r\n)+$", template: "")

		return value
	}
	
	static func splitPhoneNumber(phoneNumber: String) -> [String] {
		var arrayOfPhoneNumbers = phoneNumber.componentsSeparatedByString(";")
        let last = arrayOfPhoneNumbers.count - 1
        if arrayOfPhoneNumbers[last].isEmpty {
           arrayOfPhoneNumbers.removeAtIndex(last)
        }
		print("```````````````````````")
		print(arrayOfPhoneNumbers)
		return arrayOfPhoneNumbers
	}
}

struct Regex {
	var pattern: String {
		didSet {
			updateRegex()
		}
	}
	var expressionOptions: NSRegularExpressionOptions {
		didSet {
			updateRegex()
		}
	}
	var matchingOptions: NSMatchingOptions
	
	var regex: NSRegularExpression?
	
	init(pattern: String, expressionOptions: NSRegularExpressionOptions, matchingOptions: NSMatchingOptions) {
		self.pattern = pattern
		self.expressionOptions = expressionOptions
		self.matchingOptions = matchingOptions
		updateRegex()
	}
	
	init(pattern: String) {
		self.pattern = pattern
		expressionOptions = NSRegularExpressionOptions(rawValue: 0)
		matchingOptions = NSMatchingOptions(rawValue: 0)
		updateRegex()
	}
	
	mutating func updateRegex() {
        do {
            try regex = NSRegularExpression(pattern: pattern, options: expressionOptions)
        } catch {
            
        }
	}
}


extension String {
	func matchRegex(pattern: Regex) -> Bool {
		let range: NSRange = NSMakeRange(0, self.characters.count)
		if pattern.regex != nil {
			let matches: [AnyObject] = pattern.regex!.matchesInString(self, options: pattern.matchingOptions, range: range)
			return matches.count > 0
		}
		return false
	}
	
	func match(patternString: String) -> Bool {
		return self.matchRegex(Regex(pattern: patternString))
	}
	
	func replaceRegex(pattern: Regex, template: String) -> String {
		if self.matchRegex(pattern) {
			let range: NSRange = NSMakeRange(0, self.characters.count)
			if pattern.regex != nil {
				return pattern.regex!.stringByReplacingMatchesInString(self, options: pattern.matchingOptions, range: range, withTemplate: template)
			}
		}
		return self
	}
	
	func replace(pattern: String, template: String) -> String {
		return self.replaceRegex(Regex(pattern: pattern), template: template)
	}
}
