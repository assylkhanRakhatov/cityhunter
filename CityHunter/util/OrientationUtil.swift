//
//  OrientationUtil.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 17.05.16.
//  Copyright © 2016 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

func deviceToInterface(orientation: UIDeviceOrientation) -> UIInterfaceOrientation {
  switch orientation {
  case .Portrait:
    return UIInterfaceOrientation.Portrait
  case .PortraitUpsideDown:
    return UIInterfaceOrientation.PortraitUpsideDown
  case .LandscapeLeft:
    return UIInterfaceOrientation.LandscapeLeft
  case .LandscapeRight:
    return UIInterfaceOrientation.LandscapeRight
  default:
    return UIInterfaceOrientation.Portrait
  }
}
