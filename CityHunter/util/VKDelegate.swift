//
//  VKDelegate.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 15.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import VK_ios_sdk

class VKDelegate : NSObject, VKSdkDelegate {
  
  /**
   Notifies delegate about authorization was completed, and returns authorization result with new token or error.
   @param result contains new token or error, retrieved after VK authorization
   */
  func vkSdkAccessAuthorizationFinishedWithResult(result: VKAuthorizationResult!) {
    
  }
  
  /**
   Notifies delegate about access error, mostly connected with user deauthorized application
   */
  func vkSdkUserAuthorizationFailed() {
    
  }

	@objc func vkSdkNeedCaptchaEnter(captchaError: VKError!){
		
	}
	
	/**
	Notifies delegate about existing token has expired
	@param expiredToken old token that has expired
	*/
	@objc func vkSdkTokenHasExpired(expiredToken: VKAccessToken!){
		
	}
	
	/**
	Notifies delegate about user authorization cancelation
	@param authorizationError error that describes authorization error
	*/
	@objc func vkSdkUserDeniedAccess(authorizationError: VKError!){
		
	}
	
	/**
	Pass view controller that should be presented to user. Usually, it's an authorization window
	@param controller view controller that must be shown to user
	*/
	@objc func vkSdkShouldPresentViewController(controller: UIViewController!){
		
	}
	
	/**
	Notifies delegate about receiving new access token
	@param newToken new token for API requests
	*/
	@objc func vkSdkReceivedNewToken(newToken: VKAccessToken!){
		
	}

	
	
//	@objc var hash: Int = 0
//	
//	@objc var superclass: AnyClass?
//	
//	@objc var description: String = ""
	
	
	@objc override func isEqual(object: AnyObject?) -> Bool {
		return true
	}
	
	@objc internal override func `self`() -> Self {
		return self
	}
	
	@objc override func isProxy() -> Bool {
		return true
	}
	
	@objc override func isKindOfClass(aClass: AnyClass) -> Bool {
		return true
	}
	
	@objc override func isMemberOfClass(aClass: AnyClass) -> Bool {
		return true
	}
	
	@objc override func conformsToProtocol(aProtocol: Protocol) -> Bool {
		return true
	}
	
	@objc override func respondsToSelector(aSelector: Selector) -> Bool {
		return true
	}


	
}