//
//  CustomUIUtils.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 03.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class CustomUIUtils {
	var loadingView: UIView!
	static var instance: CustomUIUtils!
	
	static func sharedInstance() -> CustomUIUtils {
		if instance == nil {
			instance = CustomUIUtils()
		}
		return instance
	}
	
	private func createLoadingView(view: UIView, vc: UIViewController) {
		loadingView = UIView(frame: view.frame)
		loadingView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.3)
		var viewWidth:CGFloat!
		var viewHeight:CGFloat!
		
		viewWidth = view.frame.size.width * 14.0 / 16.0
		viewHeight = view.frame.size.width * 14.0 / 16.0
		
		let viewX = (CGFloat(CGRectGetWidth(UIScreen.mainScreen().bounds)) - viewWidth) / 2.0
		let viewY = (CGFloat(CGRectGetHeight(UIScreen.mainScreen().bounds)) - viewHeight) / 2.0
		let v = vc.view
		v.frame = CGRectMake(viewX, viewY-25, viewWidth, viewHeight)
		
		loadingView.addSubview(v)
	}
	
	func showLoadingView(view: UIView, vc: UIViewController) {
		createLoadingView(view, vc: vc)
		UIView.animateWithDuration(0.5, animations: { () -> Void in
			self.loadingView.alpha = 1}) { (finished) -> Void in
				view.addSubview(self.loadingView)
		}
	}
	
	func hideLoadingView() {
		UIView.animateWithDuration(0.3, animations: {
			self.loadingView.alpha = 0.0}, completion: { (done: Bool) -> Void in
				self.loadingView.removeFromSuperview()
		})
		NSNotificationCenter.defaultCenter().postNotificationName("NotificationIdentifier", object: nil)
	}

}
