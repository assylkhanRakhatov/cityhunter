//
//  AlamofireDelegate.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 28.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

protocol AlamofireDelegate {
	func handleRequest(data: NSData?, error: NSError?)
}
