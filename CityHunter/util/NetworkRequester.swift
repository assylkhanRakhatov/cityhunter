//
//  NetworkRequester.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 02.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON


class NetworkRequester {
  
  internal var alamofireDelegate: AlamofireDelegate?
  
  func getNewInstitutionsList() {
    Alamofire.request(.GET, "http://api.cityhunter.kz/posts?type=new").response { request, response, jsonData, error in
      print(request)
      print(response)
      print("error: \(error)")
      
      self.alamofireDelegate?.handleRequest(jsonData, error: error)
    }
  }
  
  func getInstitutionsList(categoryId: Int) {
    
    Alamofire.request(.GET,"http://api.cityhunter.kz/posts?category_id=\(categoryId)")
      .response { request, response, jsonData, error in
        print(request)
        print(response)
        print(error)
        
        self.alamofireDelegate?.handleRequest(jsonData, error: error)
        
    }
  }
  
  static func getInstitution(id: Int, onSuccess: (inst: Institution) -> Void, onError: (error: NSError) -> Void, onComplete: (() -> Void)? = nil) {
    Alamofire.request(.GET,"http://api.cityhunter.kz/posts/\(id)")
      .response { request, response, data, error in
        print(request)
        print(response)
        print(error)
        if let error = error {
          onError(error: error)
        } else {
          let json = JSON(data: data!)
          let inst = Institution(jsonForFull: json)
          onSuccess(inst: inst)
        }
        onComplete?()
    }
  }
  
}