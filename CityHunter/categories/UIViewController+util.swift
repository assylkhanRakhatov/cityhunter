//
//  UIViewController+util.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 16.05.16.
//  Copyright © 2016 Assylkhan Rakhatov. All rights reserved.
//

import UIKit

extension UIViewController {
  func generateCustomBarImageItem(imageTitle: String, highlightedImageTitle: String, action: () -> Void) -> UIView {
    let wrapperView = UIView(frame: CGRect(x: 0, y: 2, width: 23, height: 28))
    let imageView = ClickableImageView(image: UIImage(named: imageTitle)?.imageWithRenderingMode(.Automatic))
    imageView.enabledImage = UIImage(named: imageTitle)
    imageView.disabledImage = UIImage(named: highlightedImageTitle)
    imageView.tintColor = .whiteColor()
    imageView.userInteractionEnabled = true
    imageView.customTarget = action
    imageView.frame = CGRect(x: 4, y: 1, width: wrapperView.frame.width - 2, height: wrapperView.frame.height - 2)
    wrapperView.addSubview(imageView)
    return wrapperView
  }
  
  func greenNavbar() {
    self.navigationController?.navigationBar.backgroundColor = UIColor(red: 78.0/255.0, green: 106.0/255.0, blue: 120.0/255.0, alpha: 1.0)
    self.navigationController!.navigationBar.setBackgroundImage(nil,	forBarPosition: .Any, barMetrics:.Default)
    self.navigationController!.navigationBar.shadowImage = nil
  }

}
