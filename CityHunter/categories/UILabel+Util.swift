//
//  UILabel+Util.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 09.07.16.
//  Copyright © 2016 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
  func setTextAndSizeToFit(text: String?) {
    self.text = text
    self.sizeToFit()
  }
}
