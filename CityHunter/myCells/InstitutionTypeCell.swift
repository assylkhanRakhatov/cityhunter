//
//  InstitutionTypeViewCell.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 31.08.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class InstitutionTypeCell : UITableViewCell {

	@IBOutlet weak var instTypeImage: UIImageView!
	@IBOutlet weak var instTypeLabel: UILabel!
	
}