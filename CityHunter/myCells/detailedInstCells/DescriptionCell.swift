//
//  DescriptionCell.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 02.05.16.
//  Copyright © 2016 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class DescriptionCell: UITableViewCell {
  @IBOutlet weak var label: UILabel!
}
