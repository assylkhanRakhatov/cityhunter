//
//  PictureCell.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 02.05.16.
//  Copyright © 2016 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class PictureCell: UITableViewCell {
  var vc: DetailedInstitutionVC?
  @IBOutlet weak var wrapperView: UIView!
  @IBOutlet weak var wrapperHeight: NSLayoutConstraint!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    wrapperHeight.constant = vc!.view.frame.size.width * 9.0 / 16.0
  }
  
}
