//
//  InstitutionViewCell.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 31.08.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class InstitutionCell: UITableViewCell {
  var imageHeight: CGFloat?
  @IBOutlet weak var instThumb: UIImageView!
  @IBOutlet weak var instTitleLabel: UILabel!
  @IBOutlet weak var instAddressLabel: UILabel!
  @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var shortDescription: UILabel!
  @IBOutlet weak var recommendationHeight: NSLayoutConstraint!
  var recommendationLabel: UILabel?
  @IBOutlet weak var wrapperView: UIView!
  @IBOutlet weak var recommendationTop: NSLayoutConstraint!
  @IBOutlet weak var recommendLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    wrapperView.layer.masksToBounds = false
    wrapperView.layer.shadowColor = UIColor.blackColor().CGColor
    wrapperView.layer.shadowOffset = CGSizeMake(0.0, 0.0)
    wrapperView.layer.shadowOpacity = 0.25
    wrapperView.layer.shadowRadius = 1.0
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    if animated {
      UIView.animateWithDuration(0.3, animations: { () -> Void in
        if selected {
          self.wrapperView.backgroundColor = UIColor(white: 0.904, alpha: 1.0)
        } else {
          self.wrapperView.backgroundColor = UIColor(white: 1.0, alpha: 1.0)
        }
      })
    } else {
      if selected {
        self.wrapperView.backgroundColor = UIColor(white: 0.904, alpha: 1.0)
      } else {
        self.wrapperView.backgroundColor = UIColor(white: 1.0, alpha: 1.0)
      }
    }
  }
  
  override func setHighlighted(highlighted: Bool, animated: Bool) {
    if animated {
      UIView.animateWithDuration(0.3, animations: { () -> Void in
        if highlighted {
          self.wrapperView.backgroundColor = UIColor(white: 0.904, alpha: 1.0)
        } else {
          self.wrapperView.backgroundColor = UIColor(white: 1.0, alpha: 1.0)
        }
      })
    } else {
      if highlighted {
        self.wrapperView.backgroundColor = UIColor(white: 0.904, alpha: 1.0)
      } else {
        self.wrapperView.backgroundColor = UIColor(white: 1.0, alpha: 1.0)
      }
    }
  }
}
