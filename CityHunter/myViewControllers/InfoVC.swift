//
//  InfoVC.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 02.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class InfoVC : UIViewController {
	
	@IBOutlet weak var scImageWidth: NSLayoutConstraint!
	@IBOutlet weak var scImageHeight: NSLayoutConstraint!
	@IBOutlet weak var vkImageHeight: NSLayoutConstraint!
	@IBOutlet weak var vkImageWidth: NSLayoutConstraint!
	@IBOutlet weak var adminImageWidth: NSLayoutConstraint!
	@IBOutlet weak var adminImageHeight: NSLayoutConstraint!
	@IBOutlet weak var adminImage: UIImageView!
	@IBOutlet weak var vkImage: UIImageView!
	@IBOutlet weak var instagrammImage: UIImageView!
	@IBOutlet weak var scImage: UIImageView!
	
	override func viewDidLoad() {
//		super.viewDidLoad()
		adminImageHeight.constant = view.frame.size.width * 3.0 / 16.0
		adminImageWidth.constant = view.frame.size.width * 3.0 / 16.0
		scImageHeight.constant = view.frame.size.width * 3.0 / 16.0
		scImageWidth.constant = view.frame.size.width * 3.0 / 16.0
		
		vkImageHeight.constant = view.frame.size.width * 1.3 / 16.0
		vkImageWidth.constant = view.frame.size.width * 1.3 / 16.0

		let vkTapRecognizer = UITapGestureRecognizer(target:self, action:#selector(InfoVC.vkTapped(_:)))
		vkImage.addGestureRecognizer(vkTapRecognizer)
		vkImage.userInteractionEnabled = true
		
		let instaTapRecognizer = UITapGestureRecognizer(target:self, action:#selector(InfoVC.instaTapped(_:)))
		instagrammImage.addGestureRecognizer(instaTapRecognizer)
		instagrammImage.userInteractionEnabled = true
	}
	
	func vkTapped(img: AnyObject) {
		if let url = NSURL(string: "https://vk.com/cityhunterkrg") {
			UIApplication.sharedApplication().openURL(url)
		}
	}
	
	func instaTapped(img: AnyObject) {
		if let url = NSURL(string: "http://instagram.com/cityhunter.kz") {
			UIApplication.sharedApplication().openURL(url)
		}
	}
	
	@IBAction func cityHunterLink(sender: AnyObject) {
		if let url = NSURL(string: "http://cityhunter.kz") {
			UIApplication.sharedApplication().openURL(url)
		}
	}
	
	@IBAction func simpleCodeLink(sender: AnyObject) {
		if let url = NSURL(string: "http://www.simplecode.kz") {
			UIApplication.sharedApplication().openURL(url)
		}
	}
	
	@IBAction func closeInfoModal(sender: AnyObject) {
		CustomUIUtils.sharedInstance().hideLoadingView()
	}
}
