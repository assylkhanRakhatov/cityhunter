//
//  BaseVC.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 16.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class BaseVC: UIViewController {
	
	override func shouldAutorotate() -> Bool {
		return true
	}
	
	override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
		return UIInterfaceOrientationMask.Portrait
	}
	
	override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
		return UIInterfaceOrientation.Portrait
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		if (!isCurrentOrientationSupported()) {
			let value = preferredDeviceOrientationForPresentation().rawValue
			UIDevice.currentDevice().setValue(value, forKey: "orientation")
		}
	}
	
	private func isCurrentOrientationSupported() -> Bool {
		var deviceInterfaceOrientationMask : UIInterfaceOrientationMask
		switch (UIDevice.currentDevice().orientation) {
		case UIDeviceOrientation.Portrait:
			deviceInterfaceOrientationMask = UIInterfaceOrientationMask.Portrait
		case UIDeviceOrientation.PortraitUpsideDown:
			deviceInterfaceOrientationMask = UIInterfaceOrientationMask.PortraitUpsideDown
		case UIDeviceOrientation.LandscapeLeft:
			deviceInterfaceOrientationMask = UIInterfaceOrientationMask.LandscapeLeft
		case UIDeviceOrientation.LandscapeRight:
			deviceInterfaceOrientationMask = UIInterfaceOrientationMask.LandscapeRight
		default:
			deviceInterfaceOrientationMask = UIInterfaceOrientationMask.Portrait
		}
		return deviceInterfaceOrientationMask == supportedInterfaceOrientations()
	}
	
	private func preferredDeviceOrientationForPresentation() -> UIDeviceOrientation {
		switch (preferredInterfaceOrientationForPresentation()) {
		case UIInterfaceOrientation.Portrait:
			return UIDeviceOrientation.Portrait
		case UIInterfaceOrientation.PortraitUpsideDown:
			return UIDeviceOrientation.PortraitUpsideDown
		case UIInterfaceOrientation.LandscapeLeft:
			return UIDeviceOrientation.LandscapeLeft
		case UIInterfaceOrientation.LandscapeRight:
			return UIDeviceOrientation.LandscapeRight
		default:
			return UIDeviceOrientation.Portrait
		}
	}
	
}
