//
//  ShareVC.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 04.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit
import Social
import VK_ios_sdk

class ShareVC : UIActivityViewController, UIPopoverPresentationControllerDelegate {
	
//	@IBOutlet weak var facebookShareHeight: NSLayoutConstraint!
//	@IBOutlet weak var facebookShareWidth: NSLayoutConstraint!
//	let sharingText = "Скачай, интересно! Все досуговые заведения Караганды (рестораны, кафе, клубы, караоке…) в одном мобильном приложении"
//	
//	@IBAction func vkShare(sender: AnyObject) {
//		VKSdk.initializeWithDelegate(VKDelegate(), andAppId: "5070332")
//		var items: [AnyObject] = [sharingText] //1
//		var vkViewController: UIActivityViewController = UIActivityViewController(activityItems: items, applicationActivities: [VKActivity()])
//
//		vkViewController.setValue("VK SDK", forKey: "subject") //3
//		vkViewController.completionWithItemsHandler = nil //4
////		if (UIDevice.currentDevice().systemVersion >= "8.0") {
////			var popover: UIPopoverPresentationController = activityViewController.popoverPresentationController!
////			popover.sourceView = self.view;
//////			popover.sourceRect = [tableView rectForRowAtIndexPath:indexPath];
////		} //5
//		self.presentViewController(vkViewController, animated:true, completion:nil) //6
//	}
//	
//	@IBAction func facebookShare(sender: AnyObject) {
//		if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook) {
//			var fbShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
//			fbShare.setInitialText(sharingText)
//			self.presentViewController(fbShare, animated: true, completion: nil)
//			
//		} else {
//			var alert = UIAlertController(title: "Facebook", message: "Войдите в ваш facebook аккаунт чтобы поделиться.", preferredStyle: UIAlertControllerStyle.Alert)
//			
//			alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
//			self.presentViewController(alert, animated: true, completion: nil)
//		}
//	}
//	
//	@IBAction func twitterShare(sender: AnyObject) {
//		if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter) {
//			
//			var tweetShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
//			tweetShare.setInitialText(sharingText)
//			
//			self.presentViewController(tweetShare, animated: true, completion: nil)
//			
//		} else {
//			
//			var alert = UIAlertController(title: "Twitter", message: "Войдите в ваш twitter аккаунт чтобы поделиться.", preferredStyle: UIAlertControllerStyle.Alert)
//			
//			alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
//			
//			self.presentViewController(alert, animated: true, completion: nil)
//		}
//	}
//	
//	override func viewDidLoad() {
//		//		super.viewDidLoad()
////		static let IS_IPHONE_4_OR_LESS  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
////		static let IS_IPHONE_5          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
////		static let IS_IPHONE_6          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
////		static let IS_IPHONE_6P         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
////		static let IS_IPAD              = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
//
//		if !DeviceType.IS_IPAD {
//			facebookShareHeight.constant = CGFloat(80.0)
//			facebookShareWidth.constant = CGFloat(80.0)
//		}
//	}
//	
	func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
		return UIModalPresentationStyle.None
	}
	
	func presentationController(controller: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
		let navigationController = UINavigationController(rootViewController: controller.presentedViewController)
		let btnDone = UIBarButtonItem(title: "Done", style: .Done, target: self, action: #selector(ShareVC.dismiss))
		navigationController.topViewController!.navigationItem.rightBarButtonItem = btnDone
		return navigationController
	}
	
	func dismiss() {
		self.dismissViewControllerAnimated(true, completion: nil)
	}
//
}