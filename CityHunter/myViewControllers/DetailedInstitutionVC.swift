//
//  DetailedInstitutionVC.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 02.05.16.
//  Copyright © 2016 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class DetailedInstitutionVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
  @IBOutlet weak var tableView: UITableView!
  var shortInst: Institution?
  var fullInst: Institution?
  @IBOutlet weak var indicatorView: UIActivityIndicatorView!
  var phones: [String] = []
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.navigationBar.backgroundColor = UIColor(red: 78.0/255.0, green: 106.0/255.0, blue: 120.0/255.0, alpha: 1.0)
    self.navigationController!.navigationBar.setBackgroundImage(nil,	forBarPosition: .Any, barMetrics:.Default)
    self.navigationController!.navigationBar.shadowImage  = nil
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationItem.title = shortInst?.title
    self.navigationItem.backBarButtonItem?.title = "Назад"
    tableView.hidden = true
    NetworkRequester.getInstitution(shortInst!.id, onSuccess: { (inst) in
      self.fullInst = inst
      self.indicatorView.stopAnimating()
      self.tableView.hidden = false
      }, onError: { (error) in
        
      }, onComplete: { () in
    })
  }
}

// UITableViewDataSource
extension DetailedInstitutionVC {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    var rowsNumber = 0
    if let fullInst = fullInst {
      rowsNumber += fullInst.imageUrls.count > 0 ? 1 : 0
      rowsNumber += fullInst.description.characters.count > 0 ? 1 : 0
      
      if let info = fullInst.info {
        for inf in info {
          if inf["caption"] == "Телефон" {
            phones = StringUtil.splitPhoneNumber(inf["value"]!)
          }
        }
        if phones.count > 0 {
          rowsNumber += phones.count
          rowsNumber += info.count - 1
        } else {
          rowsNumber += info.count
        }
      }
      return rowsNumber
    }
    return 0
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
    return cell
  }
}
