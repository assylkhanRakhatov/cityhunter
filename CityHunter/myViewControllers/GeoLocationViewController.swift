//
//  GeoLocationViewController.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 03.05.16.
//  Copyright © 2016 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import AVKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class GeoLocationViewController: BaseVC, GMSMapViewDelegate, CLLocationManagerDelegate {
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var gmsMapView: GMSMapView!
  @IBOutlet weak var routeButton: UIButton!
  @IBOutlet var mapView: MKMapView!
  let locationManager = CLLocationManager()
  var inst: Institution!
  var parentOrientation: UIDeviceOrientation = UIDeviceOrientation.Portrait
  var currentLocationCoordinate: CLLocationCoordinate2D?
  var currentInstitutionCoordinate: CLLocationCoordinate2D?
  var thumb: UIImage?
  
  override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
    return UIInterfaceOrientationMask.All
  }
  
  override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    return deviceToInterface(parentOrientation)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //    self.view.bringSubviewToFront(routeButton)
    gmsMapView.delegate = self
    locationManager.delegate = self
    locationManager.requestWhenInUseAuthorization()
    greenNavbar()
    addBackButtonForModal()
    if let latitude = inst.coordinates["latitude"], let longitude = inst.coordinates["longitude"] {
      let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
      self.currentInstitutionCoordinate = coordinate
      let point = GMSMarker(position: coordinate)
      point.map = gmsMapView
      self.placeMarkersInMap(coordinate)
    }
  }
  
  @IBAction func layDirections_touchUpInside(sender: UIButton) {
    if let currentInstitutionCoordinate = self.currentInstitutionCoordinate, let currentLocationCoordinate = currentLocationCoordinate {
      self.addOverlayToMapView(currentInstitutionCoordinate, end: currentLocationCoordinate)
      //      self.generatePoints([CLLocationCoordinate2D(latitude: lat, longitude: lon), currentLocationCoordinate])
    }
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  func placeMarkersInMap(coordinate: CLLocationCoordinate2D){
    let currentMarker = GMSMarker()
    //    currentMarker.title = inst.title
    //    currentMarker.snippet = inst.address
    currentMarker.position = coordinate
    currentMarker.appearAnimation = kGMSMarkerAnimationPop
    currentMarker.icon = UIImage(named: "marker")
    currentMarker.infoWindowAnchor = CGPointMake(0.005, 0.005)
    currentMarker.map = gmsMapView
    moveCamera(currentMarker)
  }
  
  func moveCamera(marker:GMSMarker){
    gmsMapView.camera = GMSCameraPosition(target: marker.position, zoom: 15, bearing: 0, viewingAngle: 0)
  }
  
}

// CLLocationManagerDelegate
extension GeoLocationViewController {
  func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
    
    //    if status == .AuthorizedWhenInUse {
    //
    //      locationManager.startUpdatingLocation()
    //
    //      gmsMapView.myLocationEnabled = true
    //      gmsMapView.settings.myLocationButton = true
    //    }
  }
  
  func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    //    if let location = locations.first {
    //
    //      //      gmsMapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
    //
    //      self.currentLocationCoordinate = location.coordinate
    //
    //      // 8
    //      locationManager.stopUpdatingLocation()
    //    }
    
  }
  
}

// GMSMapViewDelegate
extension GeoLocationViewController {
  
  func mapView(mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
    let infoWindow = InfoWindow.instanceFromNib()
    infoWindow.name.setTextAndSizeToFit(inst.title)
    infoWindow.address.setTextAndSizeToFit(inst.address)
    //    let urlString = "http://\(inst.thumb)"
    
    //    ImageLoader.sharedLoader.imageForUrl(urlString, completionHandler: { (image, url) -> () in
    //      infoWindow.indicator.stopAnimating()
    infoWindow.pictureView.clipsToBounds = true
    infoWindow.pictureView.image = thumb
    //    })
    //    infoWindow.pictureView.image = UIImage(named: "SydneyOperaHouseAtNight")
    infoWindow.frame = CGRect(origin: infoWindow.frame.origin, size: CGSize(width: 120, height: infoWindow.frame.height))
    infoWindow.sizeToFit()
    return infoWindow
  }
  
  func mapView(mapView: GMSMapView, didTapInfoWindowOfMarker marker: GMSMarker) {
    print("didTapInfoWindowOfMarker")
  }
  
  func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
    
    let geocoder = GMSGeocoder()
    
    geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
      if let address = response?.firstResult() {
        
        let lines = address.lines
        self.addressLabel.text = lines?.joinWithSeparator("\n")
        
        let labelHeight = self.addressLabel.intrinsicContentSize().height
        let gpsButtonBottomSpace = labelHeight
        //        + self.routeButton.frame.height + 15
        self.gmsMapView.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0,
                                               bottom: gpsButtonBottomSpace, right: 0)
        
        UIView.animateWithDuration(0.25) {
          self.view.layoutIfNeeded()
        }
      }
    }
  }
  
  func mapView(mapView: GMSMapView, idleAtCameraPosition position: GMSCameraPosition) {
    reverseGeocodeCoordinate(position.target)
  }
}

// Route
extension GeoLocationViewController {
  func addPolyLineWithEncodedStringInMap(encodedString: String) {
    let path = GMSMutablePath(fromEncodedPath: encodedString)
    let polyLine = GMSPolyline(path: path)
    polyLine.strokeWidth = 5
    polyLine.strokeColor = UIColor.yellowColor()
    polyLine.map = gmsMapView
  }
  
  func addOverlayToMapView(start: CLLocationCoordinate2D, end: CLLocationCoordinate2D){
    
    let directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(start.latitude),\(start.longitude)&destination=\(end.latitude),\(end.longitude)&key=AIzaSyA8sVjLfXUJ2Mftyt3zytdqfgva-J_Vo9c&sensor=true"
    
    Alamofire.request(.GET, directionURL, parameters: nil).responseJSON { response in
      
      switch response.result {
      case .Success(let data):
        let json = JSON(data)
        if let errorMessage = json["error_message"].string {
          print(errorMessage)
        } else {
          if let routes = json["routes"].array where !routes.isEmpty {
            let overViewPolyLine = routes[0]["overview_polyline"]["points"].string
            print(overViewPolyLine)
            if let overViewPolyLine = overViewPolyLine {
              self.addPolyLineWithEncodedStringInMap(overViewPolyLine)
            }
          } else {
            let alert = UIAlertController(title: "Ошибка", message: "Не удалось проложить маршрут", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "ОК", style: .Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
          }
        }
      case .Failure(let error):
        print("Request failed with error: \(error)")
      }
    }
  }
}

