//
//  InstitutionVC.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 31.08.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import Cartography

class InstitutionsListVC : BaseVC, AlamofireDelegate, UITableViewDelegate, UITableViewDataSource {
  var filteredInstitutions: [Institution] = []
  var institutions: [Institution] = []
  var instType: InstitutionType!
  var selectedInst: Institution?
  var imageCache = [String:UIImage]()
  var networkRequester: NetworkRequester?
  var imageHeight: CGFloat!
  var selectedThumb: UIImage?
  
  @IBOutlet weak var errorLabel: UILabel!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet var indicator: UIActivityIndicatorView!
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationItem.title = "\(instType.name)"
    greenNavbar()
  }
  
  override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
    return UIInterfaceOrientationMask.Portrait
  }
  
  override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    return UIInterfaceOrientation.Portrait
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.tableView.estimatedRowHeight = 340
    self.tableView.rowHeight = UITableViewAutomaticDimension
    self.imageHeight = self.view.frame.size.width * 9.0 / 16.0
    self.tableView.hidden = true
    networkRequester = NetworkRequester()
    networkRequester?.alamofireDelegate = self
    networkRequester?.getInstitutionsList(instType.id)
  }
  
  func handleRequest(data: NSData?, error: NSError?) {
    
    if data == nil {
      print("Can't load data")
      errorLabel.hidden = false
      errorLabel.text = "Невозможно загрузить данные"
      indicator.stopAnimating()
      return
    } else if let _ = error {
      let alert = UIAlertView()
      alert.title = "Ошибка"
      alert.message = "Произошла ошибка"
      alert.addButtonWithTitle("Работа")
      alert.show()
      return
    }
    let json = JSON(data: data!)
    print(json)
    
    for (index, element) in json {
      let inst = Institution(jsonForShort: element)
      self.institutions.insert(inst, atIndex: Int(index)!)
    }
    var notRecommendedInsts:[Institution] = []
    let recommendedInsts = self.institutions.filter({if $0.isRec! {return true} else {notRecommendedInsts.append($0); return false}})
    self.filteredInstitutions.appendContentsOf(recommendedInsts)
    self.filteredInstitutions.appendContentsOf(notRecommendedInsts)
    print("list: ")
    print(self.institutions)
    self.indicator.stopAnimating()
    self.tableView.hidden = false
    self.tableView.reloadData()
  }
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "toDetailVC" {
      if let instDetailVC = segue.destinationViewController as? InstitutionInDetailVC {
        instDetailVC.shortInst = self.selectedInst
        instDetailVC.thumb = self.selectedThumb
        self.navigationItem.title = "Назад"
      }
    }
  }
  
}

// UITableViewDataSource
extension InstitutionsListVC {
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return filteredInstitutions.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCellWithIdentifier("InstCell", forIndexPath: indexPath) as! InstitutionCell
    cell.instThumb.image = nil
    let inst = filteredInstitutions[indexPath.row]
    cell.instTitleLabel.text = inst.title
    //		cell.imageHeightConstraint.constant = self.imageHeight
    let urlString = "http://\(inst.thumb)"
    
    ImageLoader.sharedLoader.imageForUrl(urlString, completionHandler: { (image, url) -> () in
      cell.instThumb.clipsToBounds = true
      cell.instThumb?.image = image
    })
    
    if let isRec = inst.isRec {
      cell.recommendLabel.hidden = !isRec
    }
    
    cell.instAddressLabel.text = inst.address
    cell.shortDescription.text = inst.shortDescription
    
    return cell
  }
  
}

// UITableViewDelegate
extension InstitutionsListVC {
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let cell = tableView.cellForRowAtIndexPath(indexPath) as! InstitutionCell
    self.selectedThumb = cell.instThumb.image
    selectedInst = filteredInstitutions[indexPath.row]
    self.performSegueWithIdentifier("toDetailVC", sender: self)
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
  }
}
