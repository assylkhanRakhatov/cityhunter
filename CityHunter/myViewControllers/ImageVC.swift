//
//  ImageVC.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 29.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class ImageVC : UIViewController {
	var imageUrl: String?
	var indicatorForImage: UIActivityIndicatorView?
	var nextVC: ImageVC?
	
	func getIndicatorInView(view: UIView) -> UIActivityIndicatorView {
		var result = UIActivityIndicatorView()
		for subview in view.subviews {
			if let indicatorView = subview as? UIActivityIndicatorView {
				result = indicatorView
			}
		}
		return result
	}
	
	override func viewDidLoad() {
		//		dispatch_async(dispatch_get_main_queue(), {
		//			self.indicatorForImage?.stopAnimating()
		//			self.indicatorForImage?.hidden = true
		//		})
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		nextVC?.viewWillAppear(true)
		indicatorForImage?.startAnimating()
		let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

//		var pageVC = self.parentViewController as! MAPageViewController
//		var nextVC: = pageVC.pageViewController(pageVC, viewControllerAfterViewController: self) as! ImageVC

		
		let url = self.imageUrl
		ImageLoader.sharedLoader.imageForUrl("http://\(url)", completionHandler: { (image, url) -> () in
			
			let imageView = self.view.subviews[0] as! UIImageView

			imageView.image = image
			self.indicatorForImage?.stopAnimating()
			
		})
		
	}
}
