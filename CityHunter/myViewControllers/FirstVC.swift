//
//  InstitutionVC.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 28.08.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit
import PageMenu
import Social
import VK_ios_sdk
import SystemConfiguration
//import Skeets

class FirstVC: BaseVC, UIViewControllerTransitioningDelegate, UIPopoverPresentationControllerDelegate {
  
	var pageMenu: CAPSPageMenu?
	var controllerArray : [UIViewController] = []
	var customUtil: CustomUIUtils?
	var shareVC: UIViewController?
	let sharingText = "Скачай, интересно! Все досуговые заведения Караганды (рестораны, кафе, клубы, караоке…) в одном мобильном приложении"
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.navigationItem.title = "City Hunter"
		self.navigationController?.navigationBar.backgroundColor = UIColor(red: 78.0/255.0, green: 106.0/255.0, blue: 120.0/255.0, alpha: 1.0)
		
		self.navigationController!.navigationBar.setBackgroundImage(UIImage(),	forBarPosition: .Any, barMetrics:.Default)
		self.navigationController!.navigationBar.shadowImage  = UIImage()
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		let paths = NSSearchPathForDirectoriesInDomains(.CachesDirectory, .UserDomainMask, true)
//		ImageManager.sharedManager.cache.diskDirectory = "\(paths[0])/ImageCache"
    
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(FirstVC.modalClosed(_:)), name:"NotificationIdentifier", object: nil)
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(FirstVC.showDetailsForCatalog(_:)), name: "userClickedToInstType", object: nil)
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(FirstVC.showDetailsForNew(_:)), name: "userClickedToNewInst", object: nil)
		
		// Array to keep track of controllers in page menu
		let b = UIBarButtonItem(image:UIImage(named:"info_icon-2"), style:.Plain, target:self, action:#selector(FirstVC.showInfo(_:)))
		let b2 = UIBarButtonItem(image:UIImage(named:"share_the"), style:.Plain, target:self, action:#selector(FirstVC.showShare(_:)))
		self.navigationItem.rightBarButtonItems = [b, b2]
		self.navigationItem.title = "City Hunter"
		
		// Create variables for all view controllers you want to put in the
		// page menu, initialize them, and add each to the controller array.
		// (Can be any UIViewController subclass)
		// Make sure the title property of all view controllers is set
		// Example:
		
		
		let catalogController = (self.storyboard?.instantiateViewControllerWithIdentifier("catalogTab"))!
		catalogController.title = "Каталог"
		let newController : UIViewController = (self.storyboard?.instantiateViewControllerWithIdentifier("newTab"))!
		newController.title = "Новое"
		controllerArray = []
		controllerArray.append(catalogController)
		controllerArray.append(newController)
		
		// Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
		// Example:
		let parameters: [CAPSPageMenuOption] = [
			.MenuItemSeparatorWidth(0),
			.UseMenuLikeSegmentedControl(true),
			.MenuItemSeparatorPercentageHeight(0.1),
			.ScrollMenuBackgroundColor(UIColor(red: 78.0/255.0, green: 106.0/255.0, blue: 120.0/255.0, alpha: 1.0))
		]
		
		// Initialize page menu with controller array, frame, and optional parameters
		pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRectMake(0.0, 64.0, self.view.frame.width, self.view.frame.height - 64.0), pageMenuOptions: parameters)
		
		
		// Lastly add page menu as subview of base view controller view
		// or use pageMenu controller in you view hierachy as desired
		self.view.addSubview(pageMenu!.view)
		
	}
	
	
	func showInfo(sender: AnyObject) {
		print("show info!")
		if shareVC != nil {
			shareVC?.dismissViewControllerAnimated(true, completion: { () -> Void in
				
			})
		}
		let infoVC = self.storyboard?.instantiateViewControllerWithIdentifier("InfoVC")
		self.addChildViewController(infoVC!)
		customUtil = CustomUIUtils.sharedInstance()
		customUtil!.showLoadingView(self.view, vc: infoVC!)
	}
	
	@IBAction func modalClosed(notification: NSNotification){
		navigationController?.view.hidden = false
	}
	
	func showShare(sender: AnyObject) {
		if customUtil != nil {
			CustomUIUtils.sharedInstance().hideLoadingView()
		}
//    VKSdk.initializeWithDelegate(VKDelegate(), andAppId: "5070332")
		VKSdk.initializeWithAppId("5070332")
		let items: [AnyObject] = [sharingText] //1
		let vkViewController: UIActivityViewController = UIActivityViewController(activityItems: items, applicationActivities: [VKActivity()])
		
		vkViewController.setValue("VK SDK", forKey: "subject") //3
		vkViewController.completionWithItemsHandler = nil //4
		
		if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone {
			self.presentViewController(vkViewController, animated: true, completion: nil)
		}	else {
			let popup: UIPopoverController = UIPopoverController(contentViewController: vkViewController)
			popup.presentPopoverFromRect(CGRectMake(self.view.frame.size.width / 2, self.view.frame.size.height / 4, 0, 0), inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
		}

	}
	
	override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
		return UIInterfaceOrientationMask.Portrait
	}
	
	override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
		return UIInterfaceOrientation.Portrait
	}
	
	func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController, sourceViewController source: UIViewController) -> UIPresentationController? {
		return HalfSizePresentationController(presentedViewController: presented, presentingViewController: presentingViewController!)
	}
	
	
	@IBAction func shareButton(sender: AnyObject) {
	}
	
	func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
		return UIModalPresentationStyle.None
	}
	
	func presentationController(controller: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
		let navigationController = UINavigationController(rootViewController: controller.presentedViewController)
		let btnDone = UIBarButtonItem(title: "Done", style: .Done, target: self, action: #selector(FirstVC.dismiss))
		navigationController.topViewController!.navigationItem.rightBarButtonItem = btnDone
		return navigationController
	}
	
	func dismiss() {
		self.dismissViewControllerAnimated(true, completion: nil)
	}
	
	@IBAction func showDetailsForCatalog(sender: NSNotification) {
		let info = sender.userInfo!["instType"] as! InstitutionType
		self.performSegueWithIdentifier("instVC", sender: self)
	}
	
	@IBAction func showDetailsForNew(sender: NSNotification) {
		let newInst = sender.userInfo!["newInst"] as! Institution
		self.performSegueWithIdentifier("newToDetail", sender: self)
	}
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		self.navigationItem.title = "Назад"
		if segue.identifier == "instVC" {
			if let instVC = segue.destinationViewController as? InstitutionsListVC {
				let selectedItemFromCatalog = (controllerArray[0] as? CatalogVC)?.selectedItem
				if selectedItemFromCatalog != nil {
					instVC.instType = selectedItemFromCatalog
					(controllerArray[0] as? CatalogVC)?.selectedItem = nil
				}
			}
		} else if segue.identifier == "newToDetail" {
			let instVC = segue.destinationViewController as? InstitutionInDetailVC
			instVC?.shortInst = (controllerArray[1] as? NewVC)?.selectedInstitution
		}
	}
}

class HalfSizePresentationController : UIPresentationController {
	override func frameOfPresentedViewInContainerView() -> CGRect {
		return CGRect(x: 0, y: 0, width: containerView!.bounds.width, height: containerView!.bounds.height/2)
	}
}
