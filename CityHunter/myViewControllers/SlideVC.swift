//
//  SlideVC.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 15.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class SlideVC : BaseVC {
  var imageUrls: [String]?
  var imageViews = [UIImageView]()
  var vcList = [UIViewController]()
  var currentImageIndex:Int?
  var currentDotId: Int?
  var pageVC: MAPageViewController?
  var parentOrientation: UIDeviceOrientation = UIDeviceOrientation.Portrait
  @IBAction func back(sender: AnyObject) {
    self.dismissViewControllerAnimated(true, completion: { () -> Void in
    })
  }
  
  @IBOutlet weak var backButton: UIButton!
  @IBOutlet weak var viewForSlide: UIView!
  
  override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    for imageUrl in self.imageUrls! {
      let imageVC = ImageVC()
      imageVC.imageUrl = imageUrl
      let myImageView = UIImageView()
      myImageView.userInteractionEnabled = true
      myImageView.makePinchable(1, maxScale: 9)
      imageVC.view.frame = self.view.frame
      imageVC.view.addSubview(myImageView)
      myImageView.translatesAutoresizingMaskIntoConstraints = false
      let views = ["image": myImageView]
      imageVC.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[image]|", options: [], metrics: nil, views: views))
      imageVC.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[image]|", options: [], metrics: nil, views: views))
      
      myImageView.contentMode = UIViewContentMode.ScaleAspectFit
      //      myImageView.frame.size = imageVC.view.frame.size
      
      vcList.append(imageVC)
    }
    
    pageVC = MAPageViewController(viewControllers: vcList)
    pageVC!.view.frame = self.view.frame
    pageVC!.view.translatesAutoresizingMaskIntoConstraints = false
    
    self.addChildViewController(pageVC!)
    self.view.addSubview(pageVC!.view)
    let views = ["pageVC": pageVC!.view]
    self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[pageVC]|", options: [], metrics: nil, views: views))
    self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[pageVC]|", options: [], metrics: nil, views: views))
    
    pageVC!.pageControl.currentPage = currentDotId!
    pageVC!.pageViewController.setViewControllers([pageVC!.viewControllers[currentImageIndex!]], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
    
    self.view.bringSubviewToFront(backButton)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SlideVC.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  func rotated()
  {
    if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation))
    {
      print("landscape")
      
    }
    
    if(UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation))
    {
      print("Portrait")
    }
    
    for vc in vcList {
      
      vc.view.frame = view.frame
      
      let imageView = vc.view.subviews[0] as! UIImageView
      
      imageView.contentMode = UIViewContentMode.ScaleAspectFit
      imageView.frame.size = vc.view.frame.size
      
    }
    
  }
  
  override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
    return UIInterfaceOrientationMask.AllButUpsideDown
  }
  
  override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    return deviceToInterface(parentOrientation)
    //    return UIInterfaceOrientation.LandscapeLeft
  }
  
}
