//
//  InstitutionInDetailVC.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 01.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import SwiftyJSON
import Cartography

class InstitutionInDetailVC : BaseVC, UIPageViewControllerDelegate {
  
  @IBOutlet weak var addressIcon: ClickableImageView!
  @IBOutlet weak var bottomViewTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var indicatorForPageVC: UIActivityIndicatorView!
  @IBOutlet weak var clockToForkBottomSpace: NSLayoutConstraint!
  @IBOutlet weak var addressToClockBottomSpace: NSLayoutConstraint!
  @IBOutlet weak var pageControllerHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var forkLabelHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var forkImageHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var clockLabelHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var clockImageHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var forkLabel: UILabel!
  @IBOutlet weak var forkImage: UIImageView!
  @IBOutlet weak var clockImage: UIImageView!
  @IBOutlet weak var clockLabel: UILabel!
  @IBOutlet weak var beforeDescriptionConstraint: NSLayoutConstraint!
  @IBOutlet weak var beforeDescriptionView: UIView!
  @IBOutlet weak var indicatorView: UIActivityIndicatorView!
  @IBOutlet weak var viewForPageViewController: UIView!
  @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var bottomView: UIView!
  @IBOutlet weak var descText: UILabel!
  @IBOutlet weak var myScrollView: UIScrollView!
  @IBOutlet weak var thumbView: UIImageView!
  var thumb: UIImage?
  @IBOutlet weak var address: UILabel!
  @IBOutlet weak var errorLabel: UILabel!
  var inst: Institution?
  var shortInst: Institution?
  var labelSumHeight: Int = 20
  var forkValue = ""
  var clockValue = ""
  var addressValue = ""
  var phoneValue = ""
  var pageVC:MAPageViewController?
  var currentNameLabel: UILabel?
  var currentValueLabel: UILabel?
  var currentImageId:Int?
  var currentDotId: Int?
  var networkRequester: NetworkRequester?
  var pricesText: String = ""
  var currentOrientation: UIDeviceOrientation = UIDeviceOrientation.Portrait
  
  override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
    return UIInterfaceOrientationMask.Portrait
  }
  
  override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    return UIInterfaceOrientation.Portrait
  }
  
  @IBAction func address_touchUpInside(sender: UIButton) {
    showLocation()
  }
  
  func showLocation() {
    performSegueWithIdentifier("showLocation", sender: nil)
  }
  
  override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
  }
  
  func rotated()
  {
    currentOrientation = UIDevice.currentDevice().orientation
    if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation))
    {
      print("landscape")
    }
    
    if(UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation))
    {
      print("Portrait")
    }
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(InstitutionInDetailVC.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
    self.navigationItem.backBarButtonItem?.title = "Назад"
    pageControllerHeightConstraint.constant = self.view.frame.size.width * 10.7 / 16
    myScrollView.hidden = true
    NetworkRequester.getInstitution(shortInst!.id, onSuccess: { (inst) in
      self.inst = inst
      self.inst?.thumb = self.shortInst!.thumb
      self.fillViews()
      self.updateViewConstraints()
      self.addPageViewController()
      }, onError: { (error) in
        let alertController = UIAlertController(title: "Ошибка сервера", message: error.localizedDescription, preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Закрыть", style: .Cancel) { (action) in
          self.navigationController?.popViewControllerAnimated(true)
        }
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
      }, onComplete: {() in
        self.indicatorView.stopAnimating()
        self.myScrollView.hidden = false
    })
  }
  
  override func viewWillAppear(animated: Bool) {
    greenNavbar()
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  func fillViews() {
    
    var count = 0
    while self.inst?.info!.count > count  {
      let caption = self.inst?.info![count]["caption"]
      let value = self.inst?.info![count]["value"]
      if let caption = caption {
        switch caption {
        case "Кухня":
          self.forkValue = value!
          self.inst?.info!.removeAtIndex(count)
          count = 0
          break
        case "Время работы":
          self.clockValue = value!
          self.inst?.info!.removeAtIndex(count)
          count = 0
          break
        case "Адрес":
          self.addressValue = value!
          self.inst?.info!.removeAtIndex(count)
          count = 0
          break
        case "Телефон":
          self.phoneValue = value!
          self.inst?.info!.removeAtIndex(count)
          count = 0
          break
        default:
          break
        }
      }
      count = count + 1
    }
    
    if !addressValue.isEmpty {
      self.address.text = addressValue
    }
    
    if !forkValue.isEmpty {
      self.forkLabel.text = forkValue
    } else {
      self.clockToForkBottomSpace.constant = CGFloat(0)
      self.forkLabelHeightConstraint.constant = CGFloat(0)
      self.forkImageHeightConstraint.constant = CGFloat(0)
      self.forkImage.hidden = true
    }
    
    if !clockValue.isEmpty {
      self.clockLabel.text = clockValue
    } else {
      self.clockToForkBottomSpace.constant = CGFloat(0)
      self.addressToClockBottomSpace.constant = CGFloat(0)
      self.clockImageHeightConstraint.constant = CGFloat(0)
      self.clockLabelHeightConstraint.constant = CGFloat(0)
      self.clockImage.hidden = true
    }
    
    if let info = self.inst!.info {
      
      for index in 0 ..< info.count {
        var item: [String: AnyObject] = info[index]
        
        let value = item["value"] as! String
        let caption = item["caption"] as! String
        
        createLabels(caption, value: value)
      }
    }
    
    var attributedDesc: NSMutableAttributedString?
    do {
      let descText = self.inst!.description.stringByAppendingString(NSString(format: "<style>body{font-family: '%@'; font-size:%fpx;}</style>", self.descText.font.familyName, self.descText.font.pointSize) as String)
      attributedDesc = try NSMutableAttributedString(data: descText.dataUsingEncoding(NSUTF8StringEncoding)!,
                                                     options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding], documentAttributes: nil)
    } catch {
      print("error ocurred")
    }
    self.descText.attributedText = attributedDesc
    self.descText.sizeToFit()
    addButtons()
    
  }
  
  func addPageViewController() {
    var vcList = [UIViewController]()
    var currentVC: ImageVC?
    let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
    dispatch_async(queue, {
      for (_, imageUrl) in self.inst!.imageUrls.enumerate() {
        let imageVC = ImageVC()
        
        let myImageView = UIImageView()
        
        imageVC.imageUrl = imageUrl
        
        imageVC.view.frame = self.viewForPageViewController.frame
        imageVC.view.addSubview(myImageView)
        currentVC?.nextVC = imageVC
        
        myImageView.contentMode = UIViewContentMode.ScaleAspectFill
        myImageView.clipsToBounds = true
        myImageView.frame.size = imageVC.view.frame.size
        
        let indicatorForImage = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicatorForImage.frame = imageVC.view.frame
        indicatorForImage.frame.origin.y = CGFloat(-10)
        indicatorForImage.color = UIColor.blackColor()
        indicatorForImage.hidesWhenStopped = true
        
        imageVC.view.addSubview(indicatorForImage)
        imageVC.indicatorForImage = indicatorForImage
        
        vcList.append(imageVC)
        
        currentVC = imageVC
      }
      
      dispatch_async(dispatch_get_main_queue(), {
        
        self.pageVC = MAPageViewController(viewControllers: vcList)
        self.pageVC?.view.frame = self.viewForPageViewController.frame
        let tap = UITapGestureRecognizer(target: self, action: #selector(InstitutionInDetailVC.openSlide(_:)))
        self.pageVC?.view.addGestureRecognizer(tap)
        
        self.addChildViewController(self.pageVC!)
        self.viewForPageViewController.addSubview(self.pageVC!.view)
        self.indicatorForPageVC.stopAnimating()
        
      });
    })
    
  }
  
  func addButtons() {
    let parentSize = self.view.frame.size
    let buttonHeight = 40
    let buttonWidth = CGFloat(parentSize.width) - 40
    var buttonSummHeight = 0
    var topButton: UIButton?
    var phoneNumbers = [String]()
    phoneNumbers = StringUtil.splitPhoneNumber(phoneValue)
    if phoneNumbers.count != 0  {
      for (index, value) in phoneNumbers.enumerate() {
        
        let button1 = UIButton()
        button1.translatesAutoresizingMaskIntoConstraints = false
        button1.setImage(UIImage(named: "phone"), forState: UIControlState.Normal)
        button1.accessibilityHint = value
        button1.setTitle("  " + value, forState: .Normal)
        button1.backgroundColor = UIColor(red: 59.0/255.0, green: 80.0/255.0, blue: 92.0/255.0, alpha: 1.0)
        button1.addTarget(self, action: #selector(InstitutionInDetailVC.phoneAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.bottomView.addSubview(button1)
        buttonSummHeight = buttonSummHeight + buttonHeight + buttonHeight/6
        
        
        if index == 0 {
          constrain(button1, self.bottomView) { btn1, btmView in
            btn1.top == btmView.top
          }
        } else {
          constrain(button1, topButton!) { b1, b2 in
            b1.top == b2.bottom + 5
          }
        }
        
        constrain(button1, self.bottomView) { b1, view in
          b1.centerX == view.centerX
          b1.width == buttonWidth
          b1.height == CGFloat(buttonHeight)
        }
        
        topButton = button1
      }
      
      self.viewHeightConstraint.constant = CGFloat(buttonSummHeight)
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "goToSlide" {
      if let slideVC = segue.destinationViewController as? SlideVC {
        slideVC.imageUrls = self.inst?.imageUrls
        slideVC.currentImageIndex = self.currentImageId
        slideVC.currentDotId = self.currentDotId
        slideVC.parentOrientation = self.currentOrientation
      }
    } else if segue.identifier == "showLocation" {
      if let nav = segue.destinationViewController as? BaseNC {
        let vc = nav.topViewController as! GeoLocationViewController
        vc.inst = self.inst!
        vc.thumb = self.thumb
        vc.parentOrientation = self.currentOrientation
      }
    }
  }
  
  func openSlide(sender: UIViewController!) {
    currentDotId = pageVC?.pageControl.currentPage
    currentImageId = pageVC?.indexOfViewController(pageVC!.viewControllers[currentDotId!])
    self.performSegueWithIdentifier("goToSlide", sender: self)
  }
  
  func phoneAction(sender: UIButton!) {
    if let text = sender.accessibilityHint {
      var trimmedText = text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
      trimmedText = trimmedText.stringByReplacingOccurrencesOfString("-", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
      trimmedText = trimmedText.stringByReplacingOccurrencesOfString("(", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
      trimmedText = trimmedText.stringByReplacingOccurrencesOfString(")", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
      if let phoneUrl:NSURL = NSURL(string:"tel://\(trimmedText)") {
        let application:UIApplication = UIApplication.sharedApplication()
        if (application.canOpenURL(phoneUrl)) {
          if UIDevice.currentDevice().userInterfaceIdiom != .Phone {
            let tapAlert = UIAlertController(title: "Ошибка!", message: "Невозможно позвонить", preferredStyle: UIAlertControllerStyle.Alert)
            tapAlert.addAction(UIAlertAction(title: "OK", style: .Destructive, handler: nil))
            self.presentViewController(tapAlert, animated: true, completion: nil)
          } else {
            application.openURL(phoneUrl)
          }
        } else {
          // your number is not valid
          let tapAlert = UIAlertController(title: "Ошибка!", message: "Неверный номер", preferredStyle: UIAlertControllerStyle.Alert)
          tapAlert.addAction(UIAlertAction(title: "OK", style: .Destructive, handler: nil))
          self.presentViewController(tapAlert, animated: true, completion: nil)
        }
      }
    }
  }
  
  func showPricesAlert() {
    let alert = UIAlertController(title: "", message: pricesText, preferredStyle: .Alert)
    alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
    self.presentViewController(alert, animated: true, completion: nil)
  }
  
  func createLabels(caption: String!, value: String!) {
    let parentSize = self.view.frame.size
    let labelHeight = 20
    let labelWidth = CGFloat(parentSize.width) - 40
    
    // name label creating
    
    let nameLabel = UILabel(frame: CGRect(x: 0, y: 0, width: Int(labelWidth), height: labelHeight))
    nameLabel.textColor = UIColor(red: 59.0/255.0, green: 80.0/255.0, blue: 92.0/255.0, alpha: 1.0)
    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    nameLabel.text = caption
    currentNameLabel = nameLabel
    self.beforeDescriptionView.addSubview(nameLabel)
    
    if currentValueLabel == nil {
      constrain(nameLabel, self.beforeDescriptionView){ label, view in
        label.top == view.top + 2
      }
    } else {
      constrain(nameLabel, self.currentValueLabel!){ l1, l2 in
        l1.top == l2.bottom + 10
      }
    }
    
    constrain(nameLabel, self.beforeDescriptionView) { label, view in
      label.leading == view.leading
      label.width == labelWidth
      label.height == CGFloat(labelHeight)
    }
    
    if caption == "Цены" {
      nameLabel.textColor = UIColor(red: 0.22, green: 0.33, blue: 0.53, alpha: 1)
      nameLabel.userInteractionEnabled = true
      nameLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(InstitutionInDetailVC.showPricesAlert)))
      self.pricesText = value
      labelSumHeight = labelSumHeight + Int(nameLabel.frame.height)
      self.beforeDescriptionConstraint.constant = CGFloat(labelSumHeight)
    } else {
      
      // value label creating
      
      let valueLabel = UILabel(frame: CGRect(x: 0, y: 0, width: Int(labelWidth), height: labelHeight))
      valueLabel.numberOfLines = 0
      
      valueLabel.text = value
      
      valueLabel.sizeToFit()
      currentValueLabel = valueLabel
      
      valueLabel.translatesAutoresizingMaskIntoConstraints = false
      
      self.beforeDescriptionView.addSubview(valueLabel)
      
      constrain(valueLabel, self.beforeDescriptionView){ label, view in
        label.leading == view.leading
        label.width == labelWidth
      }
      
      constrain(valueLabel, self.currentNameLabel!){ l1, l2 in
        l1.top == l2.bottom
      }
      
      labelSumHeight = labelSumHeight + Int(nameLabel.frame.height) + Int(valueLabel.frame.height)
      self.beforeDescriptionConstraint.constant = CGFloat(labelSumHeight)
    }
    self.updateViewConstraints()
    
  }
}
