//
//  BaseNC.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 16.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class BaseNC: UINavigationController {
	
	override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
		return topViewController!.supportedInterfaceOrientations()
	}
	
	override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
		return topViewController!.preferredInterfaceOrientationForPresentation()
	}
	
}