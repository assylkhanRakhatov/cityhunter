//
//  InstitutionTypesListVC.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 31.08.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class InstitutionTypesListVC : UITableViewController {
	
	var instTypes: [InstitutionType]!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		var type1: InstitutionType = InstitutionType(name: "Рестораны", imageName: "restaurant.png")
		var type2: InstitutionType = InstitutionType(name: "Кафе", imageName: "cafe.png")
		var type3: InstitutionType = InstitutionType(name: "Пабы и бары", imageName: "beer.png")
		var type4: InstitutionType = InstitutionType(name: "Банкетные залы", imageName: "banquet.png")
		var type5: InstitutionType = InstitutionType(name: "Бани и сауны", imageName: "bath.png")
		var type6: InstitutionType = InstitutionType(name: "Бильярд и боулинг", imageName: "bowling.png")
		var type7: InstitutionType = InstitutionType(name: "Ночные клубы и караоке", imageName: "club.png")
		var type8: InstitutionType = InstitutionType(name: "Гостиницы", imageName: "hotel.png")
		var type9: InstitutionType = InstitutionType(name: "Красота и здоровье", imageName: "beauty.png")
		var type10: InstitutionType = InstitutionType(name: "Активный отдых", imageName: "recreation.png")
		var type11: InstitutionType = InstitutionType(name: "Simple Code", imageName: "simple_code.png")
		instTypes.extend([type1, type2, type3, type4, type5, type6, type7, type8, type9, type10, type11])
	}
	
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return instTypes.count
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("InstitutionType", forIndexPath: indexPath) as! UITableViewCell
		let instType = instTypes[indexPath.row]
		cell.textLabel?.text = instType.name
		cell.imageView?.image = UIImage(named: instType.imageName)
		
	
//		if indexPath.row == selectedGameIndex {
//			cell.accessoryType = .Checkmark
//		} else {
//			cell.accessoryType = .None
//		}
		return cell
	}
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
	}
	
//	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//		if segue.identifier == "SavePlayerDetail" {
//			player = Player(name: self.nameTextField.text, game:game, rating: 1)
//		}
//		if segue.identifier == "PickGame" {
//			if let gamePickerViewController = segue.destinationViewController as? GamePickerViewController {
//				gamePickerViewController.selectedGame = game
//			}
//		}
//	}
}