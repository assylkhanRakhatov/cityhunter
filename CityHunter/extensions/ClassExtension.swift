//
//  ClassExtension.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 10.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation

extension UInt {
	init?(_ string: String, radix: UInt) {
		let digits = "0123456789abcdefghijklmnopqrstuvwxyz"
		var result = UInt(0)
		for digit in string.lowercaseString.characters {
			if let range = digits.rangeOfString(String(digit)) {
				let val = UInt(digits.startIndex.distanceTo(range.startIndex))
				if val >= radix {
					return nil
				}
				result = result * radix + val
			} else {
				return nil
			}
		}
		self = result
	}
}