//
//  UIViewController+BackButton.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 03.05.16.
//  Copyright © 2016 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
  func addButton(selectorName: String, textColor: UIColor? = .whiteColor()) {
    let customView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: 41))
//    BackButton
    let imageView = UIImageView(image: UIImage(named: "navbar_back_icon_normal")?.imageWithRenderingMode(.AlwaysOriginal))
    imageView.tintColor = textColor
    imageView.tintColorDidChange()
    customView.addSubview(imageView)
    imageView.translatesAutoresizingMaskIntoConstraints = false
    let label = UILabel(frame: CGRect.zero)
    label.text = "Назад"
    label.textColor = textColor
    customView.addSubview(label)
    label.translatesAutoresizingMaskIntoConstraints = false
    let views = ["imageView" : imageView, "label" : label]
    customView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-7-[imageView(27)]-7-|", options: [], metrics: nil, views: views))
    customView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-7-[label]-7-|", options: [], metrics: nil, views: views))
    customView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-(-10)-[imageView(25)]-(-2)-[label]|", options: [], metrics: nil, views: views))
    let quitTapRecognizer = UITapGestureRecognizer(target:self, action:Selector(selectorName))
    customView.addGestureRecognizer(quitTapRecognizer)
    customView.userInteractionEnabled = true
    navigationItem.leftBarButtonItem = UIBarButtonItem(customView: customView)
    navigationController?.navigationBar.tintColor = .whiteColor()
  }
  
  func addBackButtonForModal(textColor: UIColor? = nil) {
    addButton("backFromModal")
  }
  
  func addBackButtonForDefault() {
    addButton("back")
  }
  
  func backFromModal() {
    self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
  }
  
  func back() {
    self.navigationController!.popViewControllerAnimated(true)
  }
}
