//
//  MyRootClass.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 16.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView : NSCopying {

//	func clone() -> UIImageView! {
//		if let asCopying = ((self as AnyObject) as? NSCopying) {
//			return asCopying.copyWithZone(nil) as! UIImageView
//		}
//		else {
//			assert(false, "This class doesn't implement NSCopying")
//			return nil
//		}
//	}
	
	public func copyWithZone(zone: NSZone) -> AnyObject {
		let copy = UIImageView(image: self.image)
		copy.frame = self.frame
		return copy
	}
}
