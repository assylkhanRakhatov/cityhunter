//
//  ImageExtension.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 23.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
//	sizeChange:CGSize
	func resize(scale:CGFloat)-> UIImage {
		let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: size.width*scale, height: size.height*scale)))
		imageView.contentMode = UIViewContentMode.ScaleAspectFit
		imageView.image = self
		UIGraphicsBeginImageContext(imageView.bounds.size)
		imageView.layer.renderInContext(UIGraphicsGetCurrentContext()!)
		let result = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		return result
		
//		let hasAlpha = false
//		let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
//		
//		UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
//		self.drawInRect(CGRect(origin: CGPointZero, size: sizeChange))
//		
//		let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
//		return scaledImage
	}
	func resizeToWidth(width:CGFloat)-> UIImage {
		let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
		imageView.contentMode = UIViewContentMode.ScaleAspectFit
		imageView.image = self
		UIGraphicsBeginImageContext(imageView.bounds.size)
		imageView.layer.renderInContext(UIGraphicsGetCurrentContext()!)
		let result = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		return result
	}
}

extension UIImageView {
	public func imageFromUrl(urlString: String) {
		if let url = NSURL(string: urlString) {
			let request = NSURLRequest(URL: url)
			NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: { (response, data, error) -> Void in
                self.image = UIImage(data: data!)
            })		
		}
	}
}