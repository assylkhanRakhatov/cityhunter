//
//  VKProvider1.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 14.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation

import Foundation
import "VKSdk.h"


class VKProvider: NSObject, VKSdkDelegate {
	class func instance() -> Self {
	}
	
	func authorize() {
	}
	
	func uploadVideo(video: String, withName name: String) {
	}
	
	func vkProviderShouldPresentViewController(controller: UIViewController){
		
	}
	func vkProviderDidStartLoadingVideo(video: String) {
		
	}
	
	func vkProviderDidErrorLoadingVideo(video: String) {
		
	}
	
	func vkProviderDidEndLoadingVideo(video: String) {
		
	}
	
}

extension 

protocol VKProviderDelegate: NSObject {
	
}