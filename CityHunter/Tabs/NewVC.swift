//
//  NewVC.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 31.08.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import Cartography

class NewVC: BaseVC, AlamofireDelegate {
  var selectedInstitution: Institution?
  var institutions: [Institution] = []
  var imageCache = [String:UIImage]()
  var networkRequester: NetworkRequester?
  var imageHeight: CGFloat!
  var filteredInstitutions: [Institution] = []
  
  @IBOutlet weak var errorLabel: UILabel!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var indicator: UIActivityIndicatorView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    networkRequester = NetworkRequester()
    networkRequester?.alamofireDelegate = self
    self.imageHeight = self.view.frame.size.width * 9.0 / 16.0
    //		NSNotificationCenter.defaultCenter().addObserver(self, selector: "tabChanged:", name:"tabChanged", object: nil)
  }
  
  func handleRequest(data: NSData?, error: NSError?) {
    if data == nil {
      print("Can't load data")
      self.errorLabel.hidden = false
      self.errorLabel.text = "Невозможно загрузить данные"
      return
    } else if let _ = error {
      self.indicator.stopAnimating()
      self.errorLabel.hidden = false
      self.errorLabel.text = "Невозможно загрузить данные"
      return
    }
    
    let json = JSON(data: data!)
    
    for (index, element) in json {
      let inst = Institution(jsonForShort: element)
      self.institutions.insert(inst, atIndex: Int(index)!)
    }
    var notRecommendedInsts:[Institution] = []
    let recommendedInsts = self.institutions.filter({if $0.isRec! {return true} else {notRecommendedInsts.append($0); return false}})
    self.filteredInstitutions.appendContentsOf(recommendedInsts)
    self.filteredInstitutions.appendContentsOf(notRecommendedInsts)
    
    self.indicator.stopAnimating()
    self.tableView.hidden = false
    self.tableView.reloadData()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    errorLabel.hidden = true
    
    if self.institutions.isEmpty {
      self.tableView.hidden = true
      self.indicator.startAnimating()
      self.tableView.estimatedRowHeight = 140
      self.tableView.rowHeight = UITableViewAutomaticDimension
      networkRequester?.getNewInstitutionsList()
    }
  }
  
}

extension NewVC: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return filteredInstitutions.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("NewCell", forIndexPath: indexPath) as! InstitutionCell
    let inst = filteredInstitutions[indexPath.row] as Institution
    cell.instTitleLabel.text = inst.title
    cell.instTitleLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
    cell.instAddressLabel.text = inst.address
    cell.instAddressLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
    cell.imageHeight = self.imageHeight
    cell.imageView?.image = nil
    cell.shortDescription.text = inst.shortDescription
    cell.shortDescription.lineBreakMode = NSLineBreakMode.ByWordWrapping
    for subview in cell.subviews {
      if subview.tag == 10 {
        subview.removeFromSuperview()
      }
    }
    
    if let isRec = inst.isRec {
      cell.recommendLabel.hidden = !isRec
    }
    
    cell.layoutMargins = UIEdgeInsetsZero
    cell.preservesSuperviewLayoutMargins = false
    
    cell.imageHeightConstraint.constant = self.imageHeight
    let urlString = "http://\(inst.thumb)"
    
    ImageLoader.sharedLoader.imageForUrl(urlString, completionHandler: { (image, url) -> () in
      cell.instThumb.clipsToBounds = true
      cell.instThumb.image = image
    })
    
    return cell
    
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    self.selectedInstitution = filteredInstitutions[indexPath.row]
    NSNotificationCenter.defaultCenter().postNotificationName("userClickedToNewInst", object: nil, userInfo: ["newInst": institutions[indexPath.row]])
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
  }
  
}
