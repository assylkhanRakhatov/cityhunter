//
//  CatalogVC.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 31.08.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class CatalogVC : UIViewController {
	var instTypes: [InstitutionType] = []
	var selectedId: Int?
	var selectedItem: InstitutionType?
	var itemsCount: Int = 0
	
	@IBOutlet weak var tableView: UITableView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.navigationController?.navigationBarHidden = true
    let type1: InstitutionType = InstitutionType(id: 6, name: "Рестораны", imageName: "restaurant")
    let type2: InstitutionType = InstitutionType(id: 7, name: "Кафе", imageName: "cafe")
    let type3: InstitutionType = InstitutionType(id: 8, name: "Пабы и бары", imageName: "beer")
    let type4: InstitutionType = InstitutionType(id: 9, name: "Банкетные залы", imageName: "banquet")
    let type5: InstitutionType = InstitutionType(id: 11, name: "Бани и сауны", imageName: "bath")
    let type6: InstitutionType = InstitutionType(id: 13, name: "Бильярд и боулинг", imageName: "bowling")
    let type7: InstitutionType = InstitutionType(id: 14, name: "Ночные клубы и караоке", imageName: "club")
    let type8: InstitutionType = InstitutionType(id: 10, name: "Гостиницы", imageName: "hotel")
    let type9: InstitutionType = InstitutionType(id: 12, name: "Красота и здоровье", imageName: "beauty")
    let type10: InstitutionType = InstitutionType(id: 15, name: "Активный отдых", imageName: "recreation")
    //		let type11: InstitutionType = InstitutionType(id: 16, name: "Simple Code", imageName: "simple_code")
    instTypes = [type1, type2, type3, type4, type5, type6, type7, type8, type9, type10]
	}
	
//	@IBAction func tabChanged(sender: NSNotification) {
//		viewWillAppear(true)
//	}

	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
	}
	
	func getImageNameByName(name: String) -> String {
		switch name {
		case "Рестораны Караганды":
			return "restaurant"
		case "Кафе Караганды":
			return "cafe"
		case "Пабы бары Караганды":
			return "beer"
		case "Банкетные залы":
			return "banquet"
		case "Гостиницы Караганды":
			return "hotel"
		case "Бани сауны Караганды":
			return "bath"
		case "красота здоровье":
			return "beauty"
		case "Бильярд боулинг":
			return "bowling"
		case "Ночные клубы караоке":
			return "club"
		case "Активный отдых":
			return "recreation"
		default:
			return ""
		}
	}
}

extension CatalogVC: UITableViewDelegate, UITableViewDataSource {
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.instTypes.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("InstTypeCell", forIndexPath: indexPath) as! InstitutionTypeCell
		let instType = instTypes[indexPath.row] as InstitutionType
		cell.instTypeLabel?.text = instType.name
		cell.instTypeImage?.image = UIImage(named: instType.imageName)
		return cell
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		selectedItem = instTypes[indexPath.row]
    NSNotificationCenter.defaultCenter().postNotificationName("userClickedToInstType", object: nil, userInfo: ["instType": instTypes[indexPath.row]])
		tableView.deselectRowAtIndexPath(indexPath, animated: true)
	}
}
