//
//  ClickableImageView.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 16.05.16.
//  Copyright © 2016 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class ClickableImageView: UIImageView {
  var disabledImage: UIImage?
  var enabledImage: UIImage?
  var customTarget: (() -> Void)?
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesBegan(touches, withEvent: event)
    image = disabledImage
  }
  
  override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesEnded(touches, withEvent: event)
    image = enabledImage
    customTarget?()
  }
  
  override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
    print("touchesMoved")
    //    super.touchesMoved(touches, withEvent: event)
  }
  
  override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
    super.touchesCancelled(touches, withEvent: event)
    image = enabledImage
  }
}
