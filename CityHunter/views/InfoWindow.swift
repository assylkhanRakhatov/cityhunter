//
//  InfoWindow.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 09.07.16.
//  Copyright © 2016 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class InfoWindow: UIView {
  @IBOutlet weak var indicator: UIActivityIndicatorView!
  @IBOutlet weak var pictureView: UIImageView!
  @IBOutlet weak var name: UILabel!
  @IBOutlet weak var address: UILabel!
  class func instanceFromNib() -> InfoWindow {
    return UINib(nibName: "InfoWindow", bundle: NSBundle.mainBundle()).instantiateWithOwner(self, options: nil)[0] as! InfoWindow
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.layer.cornerRadius = 2
    self.layer.shadowColor = UIColor.blackColor().CGColor
    self.layer.shadowOpacity = 0.7
    self.layer.shadowOffset = CGSize(width: 1, height: 1)
    self.layer.shadowRadius = 3
  }
}
