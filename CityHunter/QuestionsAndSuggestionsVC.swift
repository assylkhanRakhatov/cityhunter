//
//  QuestionsAndSuggestionsVC.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 12.10.15.
//  Copyright © 2015 Assylkhan Rakhatov. All rights reserved.
//

import Foundation
import UIKit

class QuestionsAndSuggestionsVC : UIViewController {
	@IBOutlet weak var emailTextField: UITextField!
	@IBOutlet weak var messageTextField: UITextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		emailTextField.layer.borderWidth = 2
		emailTextField.layer.borderColor = UIColor(red: 182.0/255.0, green: 182.0/255.0, blue: 182.0/255.0, alpha: 1.0)

		messageTextField.layer.borderWidth = 2
		messageTextField.layer.borderColor = UIColor(red: 182.0/255.0, green: 182.0/255.0, blue: 182.0/255.0, alpha: 1.0)

	}
	
}