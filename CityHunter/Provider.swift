//
//  Provider.swift
//  CityHunter
//
//  Created by Assylkhan Rakhatov on 15.09.15.
//  Copyright (c) 2015 Assylkhan Rakhatov. All rights reserved.
//

//import Foundation
//
//import Foundation
//import VKSdk
//import VK_ios_sdk
//
//
//class VKProvider : NSObject<VKSdkDelegate> {
//
//	static var instance: VKProvider
//	var token: VKAccessToken?
//	var delegate: NSObject<VKProviderDelegate>
//	
//	func authorize() {
//		VKSdk.initializeWithDelegate(self, VK_APP_ID)
//		if VKSdk.wakeUpSession() {
//			token = VKSdk.getAccessToken()
//		} else {
//			VKSdk.authorize([VK_PER_WALL, VK_PER_PHOTOS, VK_PER_NOHTTPS, VK_PER_EMAIL, VK_PER_MESSAGES, VK_PER_VIDEO],
//				revokeAccess:YES,
//				forceOAuth:YES)
//		}
//	}
//	
//	static func instance() -> VKProvider {
//		if instance == null {
//			instance = VKProvider()
//		}
//		return instance
//	}
//
//	
// func performVKProviderShouldPresentViewController(viewController: UIViewController) {
//	if delegate.respondsToSelector("vkProviderShouldPresentViewController:"){
//		delegate.performSelector("vkProviderShouldPresentViewController:", withObject:viewController)
//	}
// }
//
//}
//
//extension VKProvider : VKSdkDelegate{
//}
//
//
//
//
//protocol VKProviderDelegate {
//
// func vkProviderShouldPresentViewController(controller: UIViewController)
//
//}



//pragma mark - Public Methods



//@property (nonatomic, strong) NSObject<VKProviderDelegate> *delegate;


//------------------------------------------

//pragma mark - Perform to delegates


//pragma mark - VK Delegates

//- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
//    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
//    [vc presentIn:self];
//	}

//	- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {
//		[self authorize];
//		}
//
//		- (void)vkSdkReceivedNewToken:(VKAccessToken *)newToken {
//			//    [self startWorking];
//			}
//
//			- (void)vkSdkAcceptedUserToken:(VKAccessToken *)token {
//				//    [self startWorking];
//				}
//				- (void)vkSdkUserDeniedAccess:(VKError *)authorizationError {
//					[[[UIAlertView alloc] initWithTitle:nil message:@"Access denied" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
//					}
//
//					- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
//						[self performVKProviderShouldPresentViewController:controller];
//						}
//